#include <Arduino.h>

#define LED_A 2
#define LED_B 4
#define LED_C 5
#define LED_D 13
#define LED_E 14
#define LED_F 15
#define LED_G 16

                  //HGFEDCBA
#define DISP_OFF  0b00000000 
#define DISP_0    0b00111111
#define DISP_1    0b00000110
#define DISP_2    0b01011011
#define DISP_3    0b01001111
#define DISP_4    0b01100110
#define DISP_5    0b01101101
#define DISP_6    0b01111101
#define DISP_7    0b00000111
#define DISP_8    0b01111111
#define DISP_9    0b01100111


const uint8_t display_pin[] = {LED_A, LED_B, LED_C, LED_D, LED_E, LED_F, LED_G};
const uint8_t display_num[] = {DISP_0, DISP_1, DISP_2, DISP_3 ,DISP_4 ,DISP_5 ,DISP_6 ,DISP_7 ,DISP_8, DISP_9, DISP_OFF};

// Advanced show
void show(uint8_t num);

// Easy show
void show_1();
void show_2();

void setup() {
  // Set pins to output
  pinMode(LED_A, OUTPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(LED_C, OUTPUT);
  pinMode(LED_D, OUTPUT);
  pinMode(LED_E, OUTPUT);
  pinMode(LED_F, OUTPUT);
  pinMode(LED_G, OUTPUT);

  // Starting state of the pins
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, HIGH);
  digitalWrite(LED_C, HIGH);
  digitalWrite(LED_D, HIGH);
  digitalWrite(LED_E, HIGH);
  digitalWrite(LED_F, HIGH);
  digitalWrite(LED_G, HIGH);


  /*
  //Do it with for loop
  for (uint8_t i = 0; i < 7; i++) {
    pinMode(display_pin[i], OUTPUT);
  }
  
  // Set start state
  for (uint8_t i = 0; i < 7; i++) {
    digitalWrite(display_pin[i], LOW);
  }
  */

}

void loop() {
  // put your main code here, to run repeatedly:
  for(uint8_t i = 0; i < 10; i++) {
    show(display_num[i]);
    delay(600);
  }
  for(uint8_t i = 8; i >= 1; i--) {
    show(display_num[i]);
    delay(600);
  }

}

void show_1()
{
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_C, LOW);
  digitalWrite(LED_D, HIGH);
  digitalWrite(LED_E, HIGH);
  digitalWrite(LED_F, HIGH);
  digitalWrite(LED_G, HIGH);
}

void show_0()
{
  digitalWrite(LED_A, LOW);
  digitalWrite(LED_B, LOW);
  digitalWrite(LED_C, LOW);
  digitalWrite(LED_D, LOW);
  digitalWrite(LED_E, LOW);
  digitalWrite(LED_F, LOW);
  digitalWrite(LED_G, HIGH);
}


// Advanced Method
void show(uint8_t num) {
  for(uint8_t i = 0; i < 7; i++) {
    if(num & 1 << i)
      digitalWrite(display_pin[i], LOW);
    else
      digitalWrite(display_pin[i], HIGH);
  }
}