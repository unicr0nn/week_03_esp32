# Week_03_ESP32

## 7 SEGMENT DISPLAY ##
![](../pics/disp_pinout.jpg)  
https://www.electronics-tutorials.ws/blog/7-segment-display-tutorial.html

## ESP32 PINOUT
![](../pics/esp32pinout.jpg)  
https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

Connect theese pins with a 100 or 220 Ohm resistors

| ESP32 PIN     | 7S Display PIN |
| ------------- | -------------- |
|        2      |       A        |
|        4      |       B        |
|        5      |       C        |
|       13      |       D        |
|       14      |       E        |
|       15      |       F        |
|       16      |       G        |
|     3.3 V     |      COM       |  