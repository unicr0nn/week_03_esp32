# Week_03_ESP32

## Settig up the enviroment
# 0 Esp32 Dev board V4 Links:
Offical website:        https://docs.espressif.com/projects/esp-idf/en/latest/hw-reference/get-started-devkitc.html  
Pinout and first steps: https://randomnerdtutorials.com/esp32-pinout-reference-gpios/  


# 1 Install Visual Studio Code: https://code.visualstudio.com/

# 2 Add PlatformIo extension to VS Code
2.1 : Go to Extensions and click on it. (Or ptrdd Ctrl + shift + x)  
2.2 : Type PlatformIO to the searching box  
2.3 : Click on PlatformIO IDE and click on instal. Meanwhile it is installing you can read the project's details.  

![](pics/01.png)

2.4 : Restart VS Code
2.5 : At Platform IO (PIO) Home Click on New Project
![](pics/02.png)

2.6 : Choose a project name, Board type (Espressif ESP32 Dev Module),  and Arduino framework. Click finish, and wait.                                 
![](pics/03.PNG)

2.7 : At Explorer window go to --> src --> main.cpp open it and write the code                                                                    
![](pics/04.png)

2.8 : Build it --> Click on PIO icon --> Under the Project Tasks click on Build                                                                              
![](pics/05.png)

2.9 : Upload it --> After the Build finished, click on the upload button. 

Enjoy the show :)