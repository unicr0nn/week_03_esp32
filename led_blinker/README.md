# Week_03_ESP32

![](../pics/ledres.jpg)

## LED BLINKER ##
Us   = 3.3 V            // Source voltage  
Ul   = 2.0 V            // Led voltage  
I    = 20 mA = 0.02 A   // Led current  
R    = (Us - Ul) / I  
R    = 65 Ohm --> Choose 100 Ohm Ressistor  