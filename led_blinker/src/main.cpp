#include <Arduino.h>

#define LED_A 2

// Function prototype
void blink_led(uint8_t pin, uint16_t d_time);


void setup() {
  // Set pin modes
  pinMode(LED_A, OUTPUT);

  // Set pins to starting state
  digitalWrite(LED_A, HIGH);
}

void loop() {
    blink_led(LED_A, 500);
}


void blink_led(uint8_t pin, uint16_t d_time)
{
  digitalWrite(pin, LOW);
  delay(d_time);
  digitalWrite(pin, HIGH);
  delay(d_time);
}
